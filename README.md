#  WindowNet: Learnable Windows for Chest X-ray Classification 

by Alessandro Wollek, Sardi Hyska, Bastian Sabel, Michael Ingrisch, and Tobias Lasser

- Official code repository
- Full paper: [https://www.mdpi.com/2589470](https://www.mdpi.com/2589470)

## Abstract

Public chest X-ray (CXR) data sets are commonly compressed to a lower bit depth to reduce their size, potentially hiding subtle diagnostic features. In contrast, radiologists apply a windowing operation to the uncompressed image to enhance such subtle features. While it has been shown that windowing improves classification performance on computed tomography (CT) images, the impact of such an operation on CXR classification performance remains unclear. In this study, we show that windowing strongly improves the CXR classification performance of machine learning models and propose WindowNet, a model that learns multiple optimal window settings. Our model achieved an average AUC score of 0.812 compared with the 0.759 score of a commonly used architecture without windowing capabilities on the MIMIC data set.
