from typing import Callable, Optional, List, Tuple
from pathlib import Path

import pytorch_lightning as pl
import torch
from torchvision import transforms as T
from torchvision.transforms import functional as TF
import pydicom

# Internal dataloader.
from ciip_dataloader.mimic import MIMIC


class Dataset(torch.utils.data.Dataset):

    def __init__(self, df, load_image: Callable, transforms: Optional[Callable] = None):
        self.df = df
        self.load_image = load_image
        self.transforms = transforms

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        target = self.df.iloc[index]
        img = self.load_image(target.Path)

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target, index


class Transforms():

    def __init__(self, labels, size, no_transforms, contrast=False, **kwargs):
        self.labels = labels
        self.size = (size, size)
        self.contrast = contrast
        self.transform = self._transform()
        self.no_transforms = no_transforms

    def _transform(self):
        # ImageNet
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
        pipeline = []
        augmentations = []
        augmentations.append(T.Normalize(mean, std))
        augmentations.append(T.Lambda(lambda x: x.div(x.max())))
        if self.contrast:
            augmentations.append(T.ColorJitter(brightness=0.5, contrast=0.5))
        pipeline.extend(augmentations)
        self.augmentations = T.Compose(augmentations)
        return T.Compose(pipeline)

    def __call__(self, input, target):
        if self.no_transforms:
            # Pass only a single image, otherwise the window convolution won't work as expected.
            return input[0][None,:], self.target_transform(target)
        return self.transform(input), self.target_transform(target)

    def target_transform(self, target):
        return target[self.labels].astype('float32').values


class DataModule(pl.LightningDataModule):

    @staticmethod
    def add_argparse_args(parent_parser):
        parser = parent_parser.add_argument_group("DataModule")
        parser.add_argument("--size", type=int, default=224)
        parser.add_argument("--num_workers", type=int, default=32)
        parser.add_argument("--batch_size", type=int, default=8)
        parser.add_argument("--pin_memory", default=True, action="store_true")
        parser.add_argument("--eight_bit", default=False, action="store_true")
        parser.add_argument("--center", default=None, type=int)
        parser.add_argument("--width", default=None, type=int)
        parser.add_argument("--samples", default=None, type=int)
        parser.add_argument("--no_transforms", default=False, action="store_true")
        parser.add_argument("--contrast_transform", default=False, action="store_true")
        return parent_parser

    @classmethod
    def from_argparse_args(cls, *args, **kwargs):
        """Pass the ArgParser's args to the constructor."""
        return pl.utilities.argparse.from_argparse_args(cls, *args, **kwargs)

    def __init__(
            self,
            batch_size,
            num_workers,
            pin_memory,
            size,
            eight_bit=False,
            center=None,
            width=None,
            cache=True,
            samples=None,
            no_transforms=False,
            contrast_transform=False,
            **kwargs
    ):
        super().__init__()
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.pin_memory = pin_memory
        self.size = size
        self.center = center
        self.width = width
        self.eight_bit = eight_bit
        self.cache = cache
        self.samples = samples
        self.no_transforms = no_transforms
        self.contrast_transform = contrast_transform

    def load_image(self, path):
        processed_path = Path(str(self.data._cache(path, dryrun=True)) + f"_size_{self.size}_and_normalized_12bit")
        if processed_path.exists():
            data = torch.load(processed_path)
        else:
            # Normalize to 0 - 4095 range (12 bit)
            print(f"file {self.data._cache(path)} not cached, transforming..")
            data = pydicom.read_file(self.data._cache(path)).pixel_array.astype("float32")
            data = TF.resize(TF.to_tensor(data), (self.size, self.size))
            data -= data.min()
            data *= 4095 / data.max()
            torch.save(data, processed_path)

        if self.eight_bit:
            data *= 255 / data.max()
            data = data.to(torch.int8).float()

        if self.center is not None:
            lower = self.center - self.width / 2
            upper = self.center + self.width / 2
            data = torch.clip(data, lower, upper)

        return torch.vstack([data, data, data])

    def setup(self, stage=None):
        self.data = MIMIC(cache_dir="/space/wollek", uncertain=0)
        self.data.load()

    def dataloader(self, df, stage):
        self.transforms = Transforms(labels=self.data.labels,
                                     size=self.size,
                                     no_transforms=self.no_transforms,
                                     contrast=self.contrast_transform)
        setattr(self, stage + "_transforms", self.transforms)
        dataset = Dataset(df, self.load_image, self.transforms)
        setattr(self, stage + "_dataset", dataset)
        return torch.utils.data.DataLoader(
            dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            shuffle=stage == "train",
        )

    def train_dataloader(self):
        return self.dataloader(self.data.df_train if self.samples is None else self.data.df_train.iloc[:self.samples], "train")

    def val_dataloader(self):
        return self.dataloader(self.data.df_val, "val")

    def test_dataloader(self):
        return self.dataloader(self.data.df_test, "test")
