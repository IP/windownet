"""INTERNAL DATALOADER! Modify as needed."""
from pathlib import Path

import pandas as pd
import numpy as np

from ciip_dataloader import Dataset
from ciip_dataloader.utils import dicom

class MIMIC(Dataset):
    """This is the MIMIC data set.

    See: https://physionet.org/content/mimic-cxr/2.0.0/
    """
    labels = [
            "Atelectasis",
            "Cardiomegaly",
            "Consolidation",
            "Edema",
            "Enlarged Cardiomediastinum",
            "Fracture",
            "Lung Lesion",
            "Lung Opacity",
            "No Finding",
            "Pleural Effusion",
            "Pleural Other",
            "Pneumonia",
            "Pneumothorax",
            "Support Devices",
    ]

    def __init__(
            self, root=None, cache_dir=None, label_format="chexpert", uncertain=0, verbose=False
    ):
        """Abstraction of the MIMIC data set.

        Parameters
        ----------
        root : Optional(str/Path)
            Path to the share-all dir (or equivalaent).
        version : str = "small"
            Dataset version.
        cach_dir : Optional(str/Path)
            Path to the cache dir, e.g. /space/your_username.
        """
        super().__init__(root=root, cache_dir=cache_dir, verbose=verbose)
        self.data_dir = self.root / "chestxray-data/mimic-cxr"
        # They updated the data set, now including DICOMS and Free Text reports but NO LABELS (yet)
        self.latest_dir = self.data_dir / "physionet.org/files/mimic-cxr/2.0.0"
        # The old version contained JPGs and LABELS in CHEXPERT and NegBIO format
        self.metadata_dir = self.data_dir / "dataset"
        if label_format not in ["chexpert", "negbio"]:
            raise ValueError(f"Labelformat {label_format} != chexpert, negbio.")
        self.label_format = label_format
        self.label_path = self.metadata_dir / f"mimic-cxr-2.0.0-{label_format}.csv"
        self.verbose = verbose
        self.uncertain = uncertain

    def load(self):
        """Load the metadata into memory."""
        self.df = pd.read_csv(self.label_path)
        self.df.fillna(0, inplace=True)

        # >>> mimic.df.groupby("split").count()["dicom_id"] / len(mimic.df)
        # split
        # test        0.013681
        # train       0.978387
        # validate    0.007932
        self.df = self.df.merge(pd.read_csv(self.metadata_dir / "mimic-cxr-2.0.0-split.csv"), on=["subject_id", "study_id"])

        # Create paths
        self.df = self.df.merge(pd.read_csv(self.latest_dir / "cxr-record-list.csv"), on=["dicom_id", "subject_id", "study_id"])
        self.df.loc[:, "Path"] = self.df.path.apply(lambda x: self.latest_dir / x)
        self.df.drop("path", axis=1, inplace=True)

        # Read metadata (also in the DICOM headers)
        # AP/PA or Lateral is quite relevant here.
        self.df = self.df.merge(pd.read_csv(self.metadata_dir / "mimic-cxr-2.0.0-metadata.csv"),
                                on=["dicom_id", "subject_id", "study_id"])
        # Replace uncertain
        self.df.replace(-1.0, self.uncertain, inplace=True)

        self.df_train = self.df.loc[self.df.split == "train"]
        self.df_val = self.df.loc[self.df.split == "validate"]
        self.df_test = self.df.loc[self.df.split == "test"]

    def load_image(self, path: str, rgb=True) -> np.array:
        """Cache and load an image."""
        return dicom.dicom_to_array(self._cache(path), rgb=rgb)
